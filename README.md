# README #

### What is this repository for? ###

Demonstrating the ability to create a peer-2-peer (p2p) JavaScript application utilizing Web RTC.

Version 0.1

### How do I get set up? ###

Just open and run the html-file on two different units or browsers, and try to create a drawing with a friend.

### Who do I talk to? ###

Rasmus Holm Andersen
mail@rasmusandersen.com